FROM node:12.8-alpine

ARG branch

# Create app directory
WORKDIR /usr/src/app

COPY package.json .
COPY package-lock.json .

RUN mkdir logs
RUN npm install
RUN npm install pm2 -g

# Bundle app source
COPY . .

EXPOSE 3000

CMD npm start && pm2 logs

