var express = require('express');
var router = express.Router();
var mongoDb = require('./mongodb');


/* GET test api */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

/* API TO GET ALL users FROM MONGODB */
router.get('/list', function (req, res, next) {
  mongoDb.connection().then(function (conn) {
      let search = {};
      mongoDb.find(conn,search).then(function (result) {
          res.status(200).send(result);
        })
        .catch(function (err) {
          console.log(err)
          res.status(500).send(err);
        })
    })
    .catch(function (err) {
      console.log(err)
      res.status(500).send(err);
    });

});

/* API TO a user FROM MONGODB */
router.get('/:userName', function (req, res, next) {
  mongoDb.connection().then(function (conn) {

      let search = {"login":req.params.userName};
      console.log(search);
      mongoDb.find(conn,search).then(function (result) {
          res.status(200).send(result);
        })
        .catch(function (err) {
          console.log(err)
          res.status(500).send(err);
        })
    })
    .catch(function (err) {
      console.log(err)
      res.status(500).send(err);
    });

});

module.exports = router;
